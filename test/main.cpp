#include <iostream>
#include "masdl.hpp"
#include <SDL/SDL.h>

using namespace MAsdl;
using namespace std;

bool done=false;
Rect d;
Display screen;
Elf fire;

class q: public Funxor{
  public:
    virtual bool OnKeyDown(SDLKey key, SDLMod mod, Uint8 state){
      switch(key){
        case SDLK_h:
        case SDLK_LEFT:
          fire["left"];
          fire.Next();
          if(fire.X()>0){
            fire.XDel(-3);
          }
          break;
        case SDLK_l:
        case SDLK_RIGHT:
          fire["right"];
          fire.Next();
          if(fire.X()<(screen.W()-24)){
            fire.XDel(3);
          }
          break;
        case SDLK_k:
        case SDLK_UP:
          fire["up"];
          fire.Next();
          if(fire.Y()>0){
            fire.YDel(-3);
          }
          break;
        case SDLK_j:
        case SDLK_DOWN:
          fire["down"];
          fire.Next();
          if(fire.Y()<(screen.H()-32)){
            fire.YDel(3);
          }
          break;
      }
    }
}qo;

class j: public Funxor{
  private:
    Funxor* target;
  public:
    j(Funxor & t){target = &t;}
    bool OnMbuttonUp(Uint8 button, Uint16 x, Uint16 y, Uint8 state){
      target->SetActive(!target->IsActive()); //'s' toggles activity
      cout << "Toggle funxor 'qo'\n";
    }
    virtual bool OnQuit(){done=true; return 0;}
};

int main(int argc, char *argv[]){
  if(Init())return 0;

  fire.Set("move.png", 24, 32, 12, 8);
  fire.ColorKey(255,0,255);
  fire["up"].SetView(0, 3, 0);
  fire["right"].SetView(1, 3, 0);
  fire["down"].SetView(2, 3, 0);
  fire["left"].SetView(3, 3, 0);

  screen.SetVideo(1200/5, 1570/5, SDL_SWSURFACE);
  screen.SetTitle("test");

  j jo(qo);
  Event event;
  event.Add(qo);
  event.Add(jo);

  screen.FRect(Rect(0,0,screen.W(),screen.H()), 10,10,100);
  SDL_EnableKeyRepeat(10,10);
  FPS fps(15);
  fps.Start();
  cerr<<fire.GetPixel(0,0);

  Font font("font.otf", 20);
  //font.Load("font.otf", 20); //"Transcends Games", a public domain font
  if(font.OK() == false)
    std::cerr<<" -- Font error: " << TTF_GetError();
  Elf *text = font.MakeText("MaSdL: cute dragons and stuff", col::Orange);
  text->Moveto(5, 30);

  while(done==false){
    fire.Draw(screen);
    text->Draw(screen);
    screen.Update();
    screen.FRect(Rect(0,0,screen.W(),screen.H()), 10,10,100);
    fps.Delay();
    while(event.Next()){}
  }
  Quit();
  return 0;
}
