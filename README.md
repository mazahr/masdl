# MAsdl

This is *MAsdl*, C++ wrapper (and maybe something more) of SDL. Its's not going to wrap just SDL, but also some extesional libraries like SDL\_image, SDL\_gfx, SDL\_mixer and so. The nonwraping work about it, is support of sprites.

It's under GNU LGPL, [`COPYING`](https://bitbucket.org/mazahr/masdl/src/master/COPYING).

It wants to be object oriented and logically glued.

[Here](http://mazahr.bitbucket.org/masdl/reference), you can check uncompleted doxygen refernece.

MAsdl is far to be finished:

- The basics are usable, but it's not as stabile as it could be (like blitting out of area).
- Sprites are almost done.
- Talking abuot extensional libraries, the only wrapped so far is SDL\_gfx (partly – only gfx\_primitives).

## Install

`$ make` creates libmasdl.so  
`$ make install` installs it (you must have permissions)  
`$ make header-install` installs header files

Now, you have to include MAsdl by writing `#include <masdl/masdl.hpp>` and, when copiling, append -lmasdl to your compiling command.
