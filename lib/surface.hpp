/*
 * MAsdl - C++ wrapper of SDL and extensional libs.
 * Copyright © 2013 Martin Zahradníček <martin.zahradniceks@gmail.com>
 *
 * This file is part of MAsdl.
 *
 * MAsdl is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MAsdl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAsdl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _MASDL_SURFACE_
#define _MASDL_SURFACE_

#include <string>
#include "face.hpp"

namespace MAsdl{

    class Surface : public Face{
        public:

            /** Creates new Surface of given resolution.
             *
             *@param width Width of creted surface.
             *@param height Height of creted surface.
             */
            Surface(int width, int height);
            Surface(SDL_Surface* source);
            Surface(const Face& other);
            Surface();
            using Face::operator=;
            Surface LoadIMG(const std::string& path)const;
            bool SelfLoad(const std::string& path);
            bool Convert(const Face& pattern);
            bool Convert();
            bool ColorKey(const int& r, const int& g, const int& b);
            bool NoColorKey();
            // Blending: 'true' means use SDL_SRCALPHA = the surface's alpha
            // will blend with the destination when blitting.
            // 'false' - no SDL_SRCALPHA = the rgba data is dumbly copied
            void BlendAlpha(bool ba = true);
    };

}

#endif
