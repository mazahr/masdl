/*
 * MAsdl - C++ wrapper of SDL and extensional libs.
 * Copyright © 2013 Martin Zahradníček <martin.zahradniceks@gmail.com>
 *
 * This file is part of MAsdl.
 *
 * MAsdl is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MAsdl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAsdl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _MASDL_FACE_
#define _MASDL_FACE_

#include <SDL/SDL_video.h>
#include "rect.hpp"
#include "color.hpp"

namespace MAsdl{

    class Face{
        protected:
            SDL_Surface* id;

            virtual void SetRect(Rect& src, bool IsNull); //This is for you, Sprites.
            const SDL_PixelFormat* GetFormat(const Face& source)const;
            Face& operator=(const Face& rhs);
        public:
            Face(int width, int height, int depth, Uint32 flags);
            explicit Face(SDL_Surface* source);
            Face(const Face& other);
            Face();
            bool Ok();//Returns true if surface is set properly.
            const int& W()const;
            const int& H()const;
            Color GetPixel(unsigned x, unsigned y);
            bool Blit(Face& source, const Rect& source_r=noRect, const Rect& destin_r=noRect);
            bool FRect(const Rect& dest, const Color& color);
            bool FRect(const Rect& dest, const short& r, const short& g, const short& b);

            /************Wraps of SDL_gfxprimitives functions***********/
            /* Pixel */
            bool Pixel(short x, short y, unsigned color);
            bool Pixel(short x, short y,  Color color);
            bool Pixel(short x, short y, char r, char g, char b, char a);
            /* Horizontal line */
            bool Hline(short x1, short x2, short y, unsigned color);
            bool Hline(short x1, short x2, short y,  Color color);
            bool Hline(short x1, short x2, short y, char r, char g, char b, char a);
            /* Vertical line */
            bool Vline(short x, short y1, short y2, unsigned color);
            bool Vline(short x, short y1, short y2,  Color color);
            bool Vline(short x, short y1, short y2, char r, char g, char b, char a);
            /* Rectangle */
            bool Rectangle(short x1, short y1, short x2, short y2, unsigned color);
            bool Rectangle(short x1, short y1, short x2, short y2,  Color color);
            bool Rectangle(short x1, short y1, short x2, short y2, char r, char g, char b, char a);
            /* Rounded-Corner Rectangle */
            bool RoundedRectangle(short x1, short y1, short x2, short y2, short rad, unsigned color);
            bool RoundedRectangle(short x1, short y1, short x2, short y2, short rad,  Color color);
            bool RoundedRectangle(short x1, short y1, short x2, short y2, short rad, char r, char g, char b, char a);
            /* Filled rectangle (Box) */
            bool Box(short x1, short y1, short x2, short y2, unsigned color);
            bool Box(short x1, short y1, short x2, short y2,  Color color);
            bool Box(short x1, short y1, short x2, short y2, char r, char g, char b, char a);
            /* Rounded-Corner Filled rectangle (Box) */
            bool RoundedBox(short x1, short y1, short x2, short y2, short rad, unsigned color);
            bool RoundedBox(short x1, short y1, short x2, short y2, short rad,  Color color);
            bool RoundedBox(short x1, short y1, short x2, short y2, short rad, char r, char g, char b, char a);
            /* Line */
            bool Line(short x1, short y1, short x2, short y2, unsigned color);
            bool Line(short x1, short y1, short x2, short y2,  Color color);
            bool Line(short x1, short y1, short x2, short y2, char r, char g, char b, char a);
            /* AA Line */
            bool AAline(short x1, short y1, short x2, short y2, unsigned color);
            bool AAline(short x1, short y1, short x2, short y2,  Color color);
            bool AAline(short x1, short y1, short x2, short y2, char r, char g, char b, char a);
            /* Thick Line */
            bool ThickLine(short x1, short y1, short x2, short y2, char width, unsigned color);
            bool ThickLine(short x1, short y1, short x2, short y2, char width,  Color color);
            bool ThickLine(short x1, short y1, short x2, short y2, char width, char r, char g, char b, char a);
            /* Circle */
            bool Circle(short x, short y, short rad, unsigned color);
            bool Circle(short x, short y, short rad,  Color color);
            bool Circle(short x, short y, short rad, char r, char g, char b, char a);
            /* Arc */
            bool Arc(short x, short y, short rad, short start, short end, unsigned color);
            bool Arc(short x, short y, short rad, short start, short end,  Color color);
            bool Arc(short x, short y, short rad, short start, short end, char r, char g, char b, char a);
            /* AA Circle */
            bool AAcircle(short x, short y, short rad, unsigned color);
            bool AAcircle(short x, short y, short rad,  Color color);
            bool AAcircle(short x, short y, short rad, char r, char g, char b, char a);
            /* Filled Circle */
            bool FilledCircle(short x, short y, short r, unsigned color);
            bool FilledCircle(short x, short y, short r,  Color color);
            bool FilledCircle(short x, short y, short rad, char r, char g, char b, char a);
            /* Ellipse */
            bool Ellipse(short x, short y, short rx, short ry, unsigned color);
            bool Ellipse(short x, short y, short rx, short ry,  Color color);
            bool Ellipse(short x, short y, short rx, short ry, char r, char g, char b, char a);
            /* AA Ellipse */
            bool AAellipse(short x, short y, short rx, short ry, unsigned color);
            bool AAellipse(short x, short y, short rx, short ry,  Color color);
            bool AAellipse(short x, short y, short rx, short ry, char r, char g, char b, char a);
            /* Filled Ellipse */
            bool FilledEllipse(short x, short y, short rx, short ry, unsigned color);
            bool FilledEllipse(short x, short y, short rx, short ry,  Color color);
            bool FilledEllipse(short x, short y, short rx, short ry, char r, char g, char b, char a);
            /* Pie */
            bool Pie(short x, short y, short rad, short start, short end, unsigned color);
            bool Pie(short x, short y, short rad, short start, short end,  Color color);
            bool Pie(short x, short y, short rad, short start, short end, char r, char g, char b, char a);
            /* Filled Pie */
            bool FilledPie(short x, short y, short rad, short start, short end, unsigned color);
            bool FilledPie(short x, short y, short rad, short start, short end,  Color color);
            bool FilledPie(short x, short y, short rad, short start, short end, char r, char g, char b, char a);
            /* Trigon */
            bool Trigon(short x1, short y1, short x2, short y2, short x3, short y3, unsigned color);
            bool Trigon(short x1, short y1, short x2, short y2, short x3, short y3,  Color color);
            bool Trigon(short x1, short y1, short x2, short y2, short x3, short y3, char r, char g, char b, char a);
            /* AA-Trigon */
            bool AAtrigon(short x1, short y1, short x2, short y2, short x3, short y3, unsigned color);
            bool AAtrigon(short x1, short y1, short x2, short y2, short x3, short y3,  Color color);
            bool AAtrigon( short x1, short y1, short x2, short y2, short x3, short y3, char r, char g, char b, char a);
            /* Filled Trigon */
            bool FilledTrigon(short x1, short y1, short x2, short y2, short x3, short y3, unsigned color);
            bool FilledTrigon(short x1, short y1, short x2, short y2, short x3, short y3,  Color color);
            bool FilledTrigon(short x1, short y1, short x2, short y2, short x3, short y3, char r, char g, char b, char a);
            /* Polygon */
            bool Polygon(const short * vx, const short * vy, int n, unsigned color);
            bool Polygon(const short * vx, const short * vy, int n,  Color color);
            bool Polygon(const short * vx, const short * vy, int n, char r, char g, char b, char a);
            /* AA-Polygon */
            bool AApolygon(const short * vx, const short * vy, int n, unsigned color);
            bool AApolygon(const short * vx, const short * vy, int n,  Color color);
            bool AApolygon(const short * vx, const short * vy, int n, char r, char g, char b, char a);
            /* Filled Polygon */
            bool FilledPolygon(const short * vx, const short * vy, int n, unsigned color);
            bool FilledPolygon(const short * vx, const short * vy, int n,  Color color);
            bool FilledPolygon(const short * vx, const short * vy, int n, char r, char g, char b, char a);
            /* Bezier */
            bool Bezier(const short * vx, const short * vy, int n, int s, unsigned color);
            bool Bezier(const short * vx, const short * vy, int n, int s,  Color color);
            bool Bezier(const short * vx, const short * vy, int n, int s, char r, char g, char b, char a);

            virtual ~Face();
            };

}

#endif
