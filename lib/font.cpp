/*
 * MAsdl - C++ wrapper of SDL and extensional libs.
 * Copyright © 2014 Adam Matoušek <adamatousek curlything gmail dot com>
 *
 * This file is part of MAsdl.
 *
 * MAsdl is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MAsdl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAsdl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "font.hpp"

namespace MAsdl{

    Font::Font()
    {
        InitMaybe();
        id = 0;
    }

    Font::Font(std::string path, unsigned size)
    {
        if(InitMaybe())
            id = TTF_OpenFont(path.c_str(), size);
        else
            id = 0;
    }

    bool Font::Load(std::string path, unsigned size)
    {
        if(id != 0)
            TTF_CloseFont(id);

        id = TTF_OpenFont(path.c_str(), size);
        return id != 0;
    }

    bool Font::InitMaybe()
    {
        if(TTF_WasInit() == false)
            return (TTF_Init() == 0);
        else
            return 1;
    }

    Elf* Font::MakeText(std::string text, Color c, bool antialiased)
    {
        if(id == 0)
            return NULL;

        SDL_Color co;
        co.r = c.r;
        co.g = c.g;
        co.b = c.b;
        //co.a = c.a; // sdl 1.2 apparently doesn't support this

        if(antialiased){
            return new Elf(Surface(TTF_RenderUTF8_Blended(id, text.c_str(), co)));
        }
        else{
            return new Elf(Surface(TTF_RenderUTF8_Solid(id, text.c_str(), co)));
        }
    }

    Font::~Font()
    {
        TTF_CloseFont(id);
    }

    bool Font::OK() const
    {
        return id != 0;
    }

    bool Font::operator!() const
    {
        return id == 0;
    }

}

