/*
 * MAsdl - C++ wrapper of SDL and extensional libs.
 * Copyright © 2013 Martin Zahradníček <martin.zahradniceks@gmail.com>
 *
 * This file is part of MAsdl.
 *
 * MAsdl is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MAsdl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAsdl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _MASDL_RECT_
#define _MASDL_RECT_

#include <SDL/SDL_video.h>

namespace MAsdl{

    class Rect{
        public:
            SDL_Rect rect;
            Sint16& x;
            Sint16& y;
            Uint16& w;
            Uint16& h;

            Rect(Sint16 sx, Sint16 sy, Uint16 sw=0, Uint16 sh=0);
            Rect();
            Rect& operator=(const Rect& rhs);
            bool operator==(const Rect& rhs)const;
    };

    extern const Rect noRect;

}

#endif
