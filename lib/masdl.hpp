/*
 * MAsdl - C++ wrapper of SDL and extensional libs.
 * Copyright © 2013 Martin Zahradníček <martin.zahradniceks@gmail.com>
 *
 * This file is part of MAsdl.
 *
 * MAsdl is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MAsdl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAsdl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _MASDL_
#define _MASDL_

#include "rect.hpp"
#include "color.hpp"
#include "face.hpp"
#include "surface.hpp"
#include "sprite.hpp"
#include "elf.hpp"
#include "display.hpp"
#include "funxor.hpp"
#include "event.hpp"
#include "fps.hpp"
#include "font.hpp"

namespace MAsdl{

    extern bool initialised;
    extern const Rect noRect;
    extern SDL_PixelFormat *DefaultAPF;

    bool Init();
    void Quit();

}

#endif
