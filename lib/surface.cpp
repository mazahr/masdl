/*
 * MAsdl - C++ wrapper of SDL and extensional libs.
 * Copyright © 2013 Martin Zahradníček <martin.zahradniceks@gmail.com>
 *
 * This file is part of MAsdl.
 *
 * MAsdl is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MAsdl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAsdl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <iostream>
#include <cmath>
#include <SDL/SDL_video.h>
#include <SDL/SDL_image.h>
#include "surface.hpp"

namespace MAsdl{

    extern SDL_PixelFormat* DefaultAPF;

    Surface::Surface(int width, int height):Face(width, height, (DefaultAPF->BitsPerPixel)/4, 0){}

    Surface::Surface(SDL_Surface* source):Face(source){}

    Surface::Surface(const Face& other):Face(other){}

    Surface::Surface(){}

    Surface Surface::LoadIMG(const std::string& path)const{
        SDL_Surface* returned, *loaded=IMG_Load(path.c_str());
        if(loaded==NULL){
            return Surface(NULL);
        }
        returned=SDL_ConvertSurface(loaded, DefaultAPF, 0);
        if(returned==NULL){
            return Surface(loaded);
        }
        SDL_SetAlpha(returned, SDL_SRCALPHA, 255);
        SDL_FreeSurface(loaded);
        return Surface(returned);
    }

    bool Surface::SelfLoad(const std::string& path){
        *this=LoadIMG(path);
    }

    bool Surface::Convert(const Face& pattern){
        SDL_Surface* old=id;
        id=SDL_ConvertSurface(old, const_cast<SDL_PixelFormat*>(GetFormat(pattern)), old->flags);
        if(id==NULL){
            id=old;
            return true;
        }
        SDL_FreeSurface(old);
        return false;
    }

    bool Surface::Convert(){
        SDL_Surface* old=id;
        id=SDL_ConvertSurface(old, DefaultAPF, old->flags);
        if(id==NULL){
            id=old;
            return true;
        }
        SDL_FreeSurface(old);
        return false;
    }

    bool Surface::ColorKey(const int& r, const int& g, const int& b){
        SDL_SetAlpha(id, 0, id->format->alpha);
        SDL_SetColorKey(id, SDL_SRCCOLORKEY, SDL_MapRGB(id->format, r, g, b));
    }

    bool Surface::NoColorKey(){
        SDL_SetColorKey(id, 0, id->format->colorkey);
    }

    void Surface::BlendAlpha(bool ba){
        if(id != NULL)
            SDL_SetAlpha(id, SDL_SRCALPHA * ba, id->format->alpha);
    }

}
