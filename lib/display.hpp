/*
 * MAsdl - C++ wrapper of SDL and extensional libs.
 * Copyright © 2013 Martin Zahradníček <martin.zahradniceks@gmail.com>
 *
 * This file is part of MAsdl.
 *
 * MAsdl is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MAsdl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAsdl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _MASDL_DISPLAY_
#define _MASDL_DISPLAY_

#include <string>
#include "rect.hpp"
#include "face.hpp"

namespace MAsdl{

    /** This class allows you to display whatever you want on the screen.
     *
     * It's nothing more (nothing less) then a Face with methods providing creating window and updating itself on it.
     */

    class Display : public Face{
        public:

            /** Creates new Display and (in the mean time) new window with given parametres.
             *
             * It calls SetVideo(), therefore all parametres are described there.
             */
            Display(int width, int height, Uint32 flags=0, Uint8 bpp=0);

            /** Creates new Diplay object with no surface assigned (no window created neither).
             */
            Display();

            /** For finding out if this Display is the displayed one.
             *
             *@return Returns true if this IS the diplayed one.
             */
            bool IsAct();

            /** Creates new window with new surface which is assigned to this object.
             *
             *@param width Sets width of the window and surface.
             *@param height Sets height of the window and surface.
             *@param flags Sets special properties of the window or video surface. It can acquire following values or OR'd combinations of them.:
             * Constant's name | Description
             * :---------------|:-----------
             * SDL_SWSURFACE | Create the video surface in system memory
             * SDL_HWSURFACE | Create the video surface in video memory
             * SDL_ASYNCBLIT | Enables the use of asynchronous updates of the display surface. This will usually slow down blitting on single CPU machines, but may provide a speed increase on SMP systems.
             * SDL_ANYFORMAT | Normally, if a video surface of the requested bits-per-pixel (bpp) is not available, SDL will emulate one with a shadow surface. Passing SDL_ANYFORMAT prevents this and causes SDL to use the video surface, regardless of its pixel depth.
             * SDL_HWPALETTE | Give SDL exclusive palette access. Without this flag you may not always get the colors you request with SDL_SetColors or SDL_SetPalette.
             * SDL_DOUBLEBUF | Enable hardware double buffering; only valid with SDL_HWSURFACE. Calling Update() will flip the buffers and update the screen. All drawing will take place on the surface that is not displayed at the moment.
             * SDL_FULLSCREEN | Will attempt to use a fullscreen mode. If a hardware resolution change is not possible (for whatever reason), the next higher resolution will be used and the display window centered on a black background.
             * SDL_OPENGL | Create an OpenGL rendering context. You should have previously set OpenGL video attributes with SDL_GL_SetAttribute.
             * SDL_OPENGLBLIT | Create an OpenGL rendering context, like above, but allow normal blitting operations. The screen (2D) surface may have an alpha channel, and SDL_UpdateRects must be used for updating changes to the screen surface. NOTE: This option is kept for compatibility only, and will be removed in next versions. Is not recommended for new code.
             * SDL_RESIZABLE | Create a resizable window. When the window is resized by the user a resize event is generated what causes (when events are checked) calling of Funxor::OnResize() and whre SetVideo() can (and should) be called again with the new size.
             * SDL_NOFRAME | If possible, SDL_NOFRAME causes creation of window with no title bar or frame decoration. Fullscreen modes automatically have this flag set.
             *@param bpp Sets number of bits per video surface's pixel  Should acquire more than 8 (incl.) and power of 2 or 24. Highly recommended not to be set. If so, then the best is set. If not, all Surfaces loaded before this calling should be converted via Surface::Convert() with no arguments given in order to be blitted faster.
             */
            bool SetVideo(int width, int height, Uint32 flags=0, Uint8 bpp=0);

            /** Displays the surface on the screen.
             *
             * If rect is used, then just the area defined by it is updated.
             * If SDL_DOUBLEBUF was set, then the buffer is flipped whole.
             *
             *@param rect Area to be updated.
             */
            bool Update(const Rect& rect=Rect(0,0,0,0));

            /** Sets text displayed as title and icon name.
             *
             *@param title Text displayed in title-bar.
             *@param icon_name Text displayed as icon name. Sometimes (often) unused.
             */
            void SetCaption(const std::string& title, const std::string& icon_name);

            /** Sets text displayed as title
             *
             *@param title Text displayed in title-bar.
             */
            void SetTitle(const std::string& title);
            //bool SetIcon(const std::string& icon_path);
    };

}

#endif
