/*
 * MAsdl - C++ wrapper of SDL and extensional libs.
 * Copyright © 2013 Martin Zahradníček <martin.zahradniceks@gmail.com>
 *
 * This file is part of MAsdl.
 *
 * MAsdl is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MAsdl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAsdl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <string>
#include <SDL/SDL_gfxPrimitives.h>
#include "face.hpp"
#include <iostream>

namespace MAsdl{

    extern SDL_PixelFormat* DefaultPF;

    void Face::SetRect(Rect& src, bool IsNull){;}

    const SDL_PixelFormat* Face::GetFormat(const Face& source)const{
        return source.id->format;
    }

    Face& Face::operator=(const Face& rhs){
        SDL_FreeSurface(id);
        id=rhs.id;
        if(id)(id->refcount)++;
        return *this;
    }

    Face::Face(int width, int height, int depth, Uint32 flags):id(SDL_CreateRGBSurface(flags, width, height, depth, DefaultPF->Rmask, DefaultPF->Gmask, DefaultPF->Bmask, DefaultPF->Amask)){}

    Face::Face(SDL_Surface* source):id(source){}

    Face::Face(const Face& other):id(other.id){(id->refcount)++;}

    Face::Face():id(NULL){}

    bool Face::Ok(){//Returns true if OK.
        if(id)return true;
        return false;
    }

    const int& Face::W()const{
        return id->w;
    }

    const int& Face::H()const{
        return id->h;
    }

    Color Face::GetPixel(unsigned x, unsigned y){
        if((x>W())||(y>H())){
            return Color(0, 0, 0);
        }
        const char* pixel = (const char*)id->pixels + y*id->pitch + x*id->format->BytesPerPixel;
        switch(id->format->BytesPerPixel){
            case 1:{
                SDL_Color col = id->format->palette->colors[*((const char*)pixel)];
                return Color(col.r, col.g, col.b);
                break;
            }
            case 2:{
                unsigned short col = *((const unsigned short*)pixel);
                return Color(((col&id->format->Rmask)>>id->format->Rshift)<<id->format->Rloss, ((col&id->format->Gmask)>>id->format->Gshift)<<id->format->Gloss, ((col&id->format->Bmask)>>id->format->Bshift)<<id->format->Bloss, ((col&id->format->Amask)>>id->format->Ashift)<<id->format->Aloss); 
                break;
            }
            case 3:{
                char r, g, b;
                r = *(pixel + id->format->Rshift/8);
                g = *(pixel + id->format->Gshift/8);
                b = *(pixel + id->format->Bshift/8);
                return Color(r, g, b);
                break;
            }
            case 4:{
                unsigned short col = *((const unsigned*)pixel);
                return Color(((col&id->format->Rmask)>>id->format->Rshift)<<id->format->Rloss, ((col&id->format->Gmask)>>id->format->Gshift)<<id->format->Gloss, ((col&id->format->Bmask)>>id->format->Bshift)<<id->format->Bloss, ((col&id->format->Amask)>>id->format->Ashift)<<id->format->Aloss); 
                break;
            }
            default:{
                return Color(0, 0, 0);
                break;
            }
        }
    }

    bool Face::Blit(Face& source, const Rect& source_r, const Rect& dest_r){
        if((!source.Ok())||(!Ok()))
            return false;
        Rect src_r;
        src_r=source_r;
        source.SetRect(src_r, (&source_r==&noRect?true:false));
        return (SDL_BlitSurface(source.id, (&source_r==&noRect?(src_r==noRect?NULL:&(src_r.rect)):const_cast<SDL_Rect*>(&(source_r.rect))), id, (&dest_r==&noRect?NULL:const_cast<SDL_Rect*>(&(dest_r.rect))))?true:false);
    }

    bool Face::FRect(const Rect& dest, const Color& color){
        return (SDL_FillRect(id, const_cast<SDL_Rect*>(&(dest.rect)), SDL_MapRGBA(id->format, color.r, color.g, color.b, color.a))?true:false);
    }

    bool Face::FRect(const Rect& dest, const short& r, const short& g, const short& b){FRect(dest, Color(r, g, b, 255));}


    /* Pixel */
    bool Face::Pixel(short x, short y, unsigned color){
        pixelColor(id, x, y, color);
    }
    bool Face::Pixel(short x, short y, Color color){
        pixelRGBA(id, x, y, color.r, color.g, color.b, color.a);
    }
    bool Face::Pixel(short x, short y, char r, char g, char b, char a){
        pixelRGBA(id, x, y, r, g, b, a);
    }

    /* Horizontal line */
    bool Face::Hline(short x1, short x2, short y, unsigned color){
        hlineColor(id, x1, x2, y, color);
    }
    bool Face::Hline(short x1, short x2, short y, Color color){
        hlineRGBA(id, x1, x2, y, color.r, color.g, color.b, color.a);
    }
    bool Face::Hline(short x1, short x2, short y, char r, char g, char b, char a){
        hlineRGBA(id, x1, x2, y, r, g, b, a);
    }

    /* Vertical line */
    bool Face::Vline(short x, short y1, short y2, unsigned color){
        vlineColor(id, x, y1, y2, color);
    }
    bool Face::Vline(short x, short y1, short y2, Color color){
        vlineRGBA(id, x, y1, y2, color.r, color.g, color.b, color.a);
    }
    bool Face::Vline(short x, short y1, short y2, char r, char g, char b, char a){
        vlineRGBA(id, x, y1, y2, r, g, b, a);
    }

    /* Rectangle */
    bool Face::Rectangle(short x1, short y1, short x2, short y2, unsigned color){
        rectangleColor(id, x1, y1, x2, y2, color);
    }
    bool Face::Rectangle(short x1, short y1, short x2, short y2, Color color){
        rectangleRGBA(id, x1, y1, x2, y2, color.r, color.g, color.b, color.a);
    }
    bool Face::Rectangle(short x1, short y1, short x2, short y2, char r, char g, char b, char a){
        rectangleRGBA(id, x1, y1, x2, y2, r, g, b, a);
    }

    /* Rounded-Corner Rectangle */
    bool Face::RoundedRectangle(short x1, short y1, short x2, short y2, short rad, unsigned color){
        roundedRectangleColor(id, x1, y1, x2, y2, rad, color);
    }
    bool Face::RoundedRectangle(short x1, short y1, short x2, short y2, short rad, Color color){
        roundedRectangleRGBA(id, x1, y1, x2, y2, rad, color.r, color.g, color.b, color.a);
    }
    bool Face::RoundedRectangle(short x1, short y1, short x2, short y2, short rad, char r, char g, char b, char a){
        roundedRectangleRGBA(id, x1, y1, x2, y2, rad, r, g, b, a);
    }

    /* Filled rectangle (Box) */
    bool Face::Box(short x1, short y1, short x2, short y2, unsigned color){
        boxColor(id, x1, y1, x2, y2, color);
    }
    bool Face::Box(short x1, short y1, short x2, short y2, Color color){
        boxRGBA(id, x1, y1, x2, y2, color.r, color.g, color.b, color.a);
    }
    bool Face::Box(short x1, short y1, short x2, short y2, char r, char g, char b, char a){
        boxRGBA(id, x1, y1, x2, y2, r, g, b, a);
    }

    /* Rounded-Corner Filled rectangle (Box) */
    bool Face::RoundedBox(short x1, short y1, short x2, short y2, short rad, unsigned color){
        roundedBoxColor(id, x1, y1, x2, y2, rad, color);
    }
    bool Face::RoundedBox(short x1, short y1, short x2, short y2, short rad, Color color){
        roundedBoxRGBA(id, x1, y1, x2, y2, rad, color.r, color.g, color.b, color.a);
    }
    bool Face::RoundedBox(short x1, short y1, short x2, short y2, short rad, char r, char g, char b, char a){
        roundedBoxRGBA(id, x1, y1, x2, y2, rad, r, g, b, a);
    }

    /* Line */
    bool Face::Line(short x1, short y1, short x2, short y2, unsigned color){
        lineColor(id, x1, y1, x2, y2, color);
    }
    bool Face::Line(short x1, short y1, short x2, short y2, Color color){
        lineRGBA(id, x1, y1, x2, y2, color.r, color.g, color.b, color.a);
    }
    bool Face::Line(short x1, short y1, short x2, short y2, char r, char g, char b, char a){
        lineRGBA(id, x1, y1, x2, y2, r, g, b, a);
    }

    /* AA Line */
    bool Face::AAline(short x1, short y1, short x2, short y2, unsigned color){
        aalineColor(id, x1, y1, x2, y2, color);
    }
    bool Face::AAline(short x1, short y1, short x2, short y2, Color color){
        aalineRGBA(id, x1, y1, x2, y2, color.r, color.g, color.b, color.a);
    }
    bool Face::AAline(short x1, short y1, short x2, short y2, char r, char g, char b, char a){
        aalineRGBA(id, x1, y1, x2, y2, r, g, b, a);
    }

    /* Thick Line */
    bool Face::ThickLine(short x1, short y1, short x2, short y2, char width, unsigned color){
        thickLineColor(id, x1, y1, x2, y2, width, color);
    }
    bool Face::ThickLine(short x1, short y1, short x2, short y2, char width, Color color){
        thickLineRGBA(id, x1, y1, x2, y2, width, color.r, color.g, color.b, color.a);
    }
    bool Face::ThickLine(short x1, short y1, short x2, short y2, char width, char r, char g, char b, char a){
        thickLineRGBA(id, x1, y1, x2, y2, width, r, g, b, a);
    }

    /* Circle */
    bool Face::Circle(short x, short y, short rad, unsigned color){
        circleColor(id, x, y, rad, color);
    }
    bool Face::Circle(short x, short y, short rad, Color color){
        circleRGBA(id, x, y, rad, color.r, color.g, color.b, color.a);
    }
    bool Face::Circle(short x, short y, short rad, char r, char g, char b, char a){
        circleRGBA(id, x, y, rad, r, g, b, a);
    }

    /* Arc */
    bool Face::Arc(short x, short y, short rad, short start, short end, unsigned color){
        arcColor(id, x, y, rad, start, end, color);
    }
    bool Face::Arc(short x, short y, short rad, short start, short end, Color color){
        arcRGBA(id, x, y, rad, start, end, color.r, color.g, color.b, color.a);
    }
    bool Face::Arc(short x, short y, short rad, short start, short end, char r, char g, char b, char a){
        arcRGBA(id, x, y, rad, start, end, r, g, b, a);
    }

    /* AA Circle */
    bool Face::AAcircle(short x, short y, short rad, unsigned color){
        aacircleColor(id, x, y, rad, color);
    }
    bool Face::AAcircle(short x, short y, short rad, Color color){
        aacircleRGBA(id, x, y, rad, color.r, color.g, color.b, color.a);
    }
    bool Face::AAcircle(short x, short y, short rad, char r, char g, char b, char a){
        aacircleRGBA(id, x, y, rad, r, g, b, a);
    }

    /* Filled Circle */
    bool Face::FilledCircle(short x, short y, short r, unsigned color){
        filledCircleColor(id, x, y, r, color);
    }
    bool Face::FilledCircle(short x, short y, short r, Color color){
        filledCircleRGBA(id, x, y, r, color.r, color.g, color.b, color.a);
    }
    bool Face::FilledCircle(short x, short y, short rad, char r, char g, char b, char a){
        filledCircleRGBA(id, x, y, rad, r, g, b, a);
    }

    /* Ellipse */
    bool Face::Ellipse(short x, short y, short rx, short ry, unsigned color){
        ellipseColor(id, x, y, rx, ry, color);
    }
    bool Face::Ellipse(short x, short y, short rx, short ry, Color color){
        ellipseRGBA(id, x, y, rx, ry, color.r, color.g, color.b, color.a);
    }
    bool Face::Ellipse(short x, short y, short rx, short ry, char r, char g, char b, char a){
        ellipseRGBA(id, x, y, rx, ry, r, g, b, a);
    }

    /* AA Ellipse */
    bool Face::AAellipse(short x, short y, short rx, short ry, unsigned color){
        aaellipseColor(id, x, y, rx, ry, color);
    }
    bool Face::AAellipse(short x, short y, short rx, short ry, Color color){
        aaellipseRGBA(id, x, y, rx, ry, color.r, color.g, color.b, color.a);
    }
    bool Face::AAellipse(short x, short y, short rx, short ry, char r, char g, char b, char a){
        aaellipseRGBA(id, x, y, rx, ry, r, g, b, a);
    }

    /* Filled Ellipse */
    bool Face::FilledEllipse(short x, short y, short rx, short ry, unsigned color){
        filledEllipseColor(id, x, y, rx, ry, color);
    }
    bool Face::FilledEllipse(short x, short y, short rx, short ry, Color color){
        filledEllipseRGBA(id, x, y, rx, ry, color.r, color.g, color.b, color.a);
    }
    bool Face::FilledEllipse(short x, short y, short rx, short ry, char r, char g, char b, char a){
        filledEllipseRGBA(id, x, y, rx, ry, r, g, b, a);
    }

    /* Pie */
    bool Face::Pie(short x, short y, short rad, short start, short end, unsigned color){
        pieColor(id, x, y, rad, start, end, color);
    }
    bool Face::Pie(short x, short y, short rad, short start, short end, Color color){
        pieRGBA(id, x, y, rad, start, end, color.r, color.g, color.b, color.a);
    }
    bool Face::Pie(short x, short y, short rad, short start, short end, char r, char g, char b, char a){
        pieRGBA(id, x, y, rad, start, end, r, g, b, a);
    }

    /* Filled Pie */
    bool Face::FilledPie(short x, short y, short rad, short start, short end, unsigned color){
        filledPieColor(id, x, y, rad, start, end, color);
    }
    bool Face::FilledPie(short x, short y, short rad, short start, short end, Color color){
        filledPieRGBA(id, x, y, rad, start, end, color.r, color.g, color.b, color.a);
    }
    bool Face::FilledPie(short x, short y, short rad, short start, short end, char r, char g, char b, char a){
        filledPieRGBA(id, x, y, rad, start, end, r, g, b, a);
    }

    /* Trigon */
    bool Face::Trigon(short x1, short y1, short x2, short y2, short x3, short y3, unsigned color){
        trigonColor(id, x1, y1, x2, y2, x3, y3, color);
    }
    bool Face::Trigon(short x1, short y1, short x2, short y2, short x3, short y3, Color color){
        trigonRGBA(id, x1, y1, x2, y2, x3, y3, color.r, color.g, color.b, color.a);
    }
    bool Face::Trigon(short x1, short y1, short x2, short y2, short x3, short y3, char r, char g, char b, char a){
        trigonRGBA(id, x1, y1, x2, y2, x3, y3, r, g, b, a);
    }

    /* AA-Trigon */
    bool Face::AAtrigon(short x1, short y1, short x2, short y2, short x3, short y3, unsigned color){
        aatrigonColor(id, x1, y1, x2, y2, x3, y3, color);
    }
    bool Face::AAtrigon(short x1, short y1, short x2, short y2, short x3, short y3, Color color){
        aatrigonRGBA(id, x1, y1, x2, y2, x3, y3, color.r, color.g, color.b, color.a);
    }
    bool Face::AAtrigon( short x1, short y1, short x2, short y2, short x3, short y3, char r, char g, char b, char a){
        aatrigonRGBA(id, x1, y1, x2, y2, x3, y3, r, g, b, a);
    }

    /* Filled Trigon */
    bool Face::FilledTrigon(short x1, short y1, short x2, short y2, short x3, short y3, unsigned color){
        filledTrigonColor(id, x1, y1, x2, y2, x3, y3, color);
    }
    bool Face::FilledTrigon(short x1, short y1, short x2, short y2, short x3, short y3, Color color){
        filledTrigonRGBA(id, x1, y1, x2, y2, x3, y3, color.r, color.g, color.b, color.a);
    }
    bool Face::FilledTrigon(short x1, short y1, short x2, short y2, short x3, short y3, char r, char g, char b, char a){
        filledTrigonRGBA(id, x1, y1, x2, y2, x3, y3, r, g, b, a);
    }

    /* Polygon */
    bool Face::Polygon(const short * vx, const short * vy, int n, unsigned color){
        polygonColor(id, vx, vy, n, color);
    }
    bool Face::Polygon(const short * vx, const short * vy, int n, Color color){
        polygonRGBA(id, vx, vy, n, color.r, color.g, color.b, color.a);
    }
    bool Face::Polygon(const short * vx, const short * vy, int n, char r, char g, char b, char a){
        polygonRGBA(id, vx, vy, n, r, g, b, a);
    }

    /* AA-Polygon */
    bool Face::AApolygon(const short * vx, const short * vy, int n, unsigned color){
        aapolygonColor(id, vx, vy, n, color);
    }
    bool Face::AApolygon(const short * vx, const short * vy, int n, Color color){
        aapolygonRGBA(id, vx, vy, n, color.r, color.g, color.b, color.a);
    }
    bool Face::AApolygon(const short * vx, const short * vy, int n, char r, char g, char b, char a){
        aapolygonRGBA(id, vx, vy, n, r, g, b, a);
    }

    /* Filled Polygon */
    bool Face::FilledPolygon(const short * vx, const short * vy, int n, unsigned color){
        filledPolygonColor(id, vx, vy, n, color);
    }
    bool Face::FilledPolygon(const short * vx, const short * vy, int n, Color color){
        filledPolygonRGBA(id, vx, vy, n, color.r, color.g, color.b, color.a);
    }
    bool Face::FilledPolygon(const short * vx, const short * vy, int n, char r, char g, char b, char a){
        filledPolygonRGBA(id, vx, vy, n, r, g, b, a);
    }

    /* Bezier */
    bool Face::Bezier(const short * vx, const short * vy, int n, int s, unsigned color){
        bezierColor(id, vx, vy, n, s, color);
    }
    bool Face::Bezier(const short * vx, const short * vy, int n, int s, Color color){
        bezierRGBA(id, vx, vy, n, s, color.r, color.g, color.b, color.a);
    }
    bool Face::Bezier(const short * vx, const short * vy, int n, int s, char r, char g, char b, char a){
        bezierRGBA(id, vx, vy, n, s, r, g, b, a);
    }


    Face::~Face(){
        SDL_FreeSurface(id);
    }

}
