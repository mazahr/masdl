/*
 * MAsdl - C++ wrapper of SDL and extensional libs.
 * Copyright © 2013 Martin Zahradníček <martin.zahradniceks@gmail.com>
 *
 * This file is part of MAsdl.
 *
 * MAsdl is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MAsdl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAsdl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "elf.hpp"

namespace MAsdl{

    Elf::Elf(const Sprite& other) : Sprite(other), x(0), y(0){;}

    Elf::Elf(const Surface& other, const unsigned& frame_width, const unsigned& frame_height, const unsigned short& frames_in_x, const unsigned short& frames_in_y, bool hrs) : Sprite(other, frame_width, frame_height, frames_in_x, frames_in_y, hrs), x(0), y(0){;}

    Elf::Elf(const std::string path, const unsigned& frame_width, const unsigned& frame_height, const unsigned short& frames_in_x, const unsigned short& frames_in_y, bool hrs) : Sprite(path, frame_width, frame_height, frames_in_x, frames_in_y, hrs), x(0), y(0){;}

    Elf::Elf() : Sprite(), x(0), y(0){;}

    void Elf::Moveto(int x_dest, int y_dest){
        X(x_dest);
        Y(y_dest);
    }

    void Elf::Move(int x_delta, int y_delta){
        XDel(x_delta);
        YDel(y_delta);
    }

    const int& Elf::X(){
        return x;
    }

    const int& Elf::X(int x_dest){
        x=x_dest;
        return x;
    }

    const int& Elf::XDel(int x_delta){
        x+=x_delta;
        return x;
    }

    const int& Elf::Y(){
        return y;
    }

    const int& Elf::Y(int y_dest){
        y=y_dest;
        return y;
    }

    const int& Elf::YDel(int y_delta){
        y+=y_delta;
        return y;
    }

    bool Elf::Draw(Face destination){
        return destination.Blit(*this, noRect, Rect(x, y, 0, 0));
    }

}
