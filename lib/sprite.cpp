/*
 * MAsdl - C++ wrapper of SDL and extensional libs.
 * Copyright © 2013 Martin Zahradníček <martin.zahradniceks@gmail.com>
 *
 * This file is part of MAsdl.
 *
 * MAsdl is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MAsdl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAsdl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sprite.hpp"

namespace MAsdl{

    Sprite::View::View(unsigned short s_line, unsigned short s_frame_count, unsigned short s_frame_no){
        line=s_line;
        frame_count=s_frame_count;
        frame_no=s_frame_no;
    }

    void Sprite::SetRect(Rect& src, bool IsNull){
        if(IsNull){
            if(horisontal){
                src.x=(recent_view->frame_no % frames_x)*frame_w;
                src.y=(recent_view->line + (recent_view->frame_no / frames_x))*frame_h;
            }
            else{
                src.x=(recent_view->line + (recent_view->frame_no / frames_y))*frame_w;
                src.y=(recent_view->frame_no % frames_y) * frame_h;
            }
            src.w=frame_w;
            src.h=frame_h;
            return;
        }
        if(horisontal){
            src.x+=(recent_view->frame_no % frames_x)*frame_w;
            src.y+=(recent_view->line + (recent_view->frame_no / frames_x))*frame_h;
        }
        else{
            src.x+=(recent_view->line + (recent_view->frame_no / frames_y))*frame_w;
            src.y+=(recent_view->frame_no % frames_y) * frame_h;
        }
        return;
    }

    Sprite::Sprite(const Sprite& other) : Surface(other), recent_view(NULL){
        frame_w=other.frame_w;
        frame_h=other.frame_h;
        frames_x=other.frames_x;
        frames_y=other.frames_y;
        horisontal=other.horisontal;
        recent_view=&views["default"];
        recent_view->frame_count=(frames_x*frames_y);
    }

    Sprite::Sprite(const Surface& other) : Surface(other), recent_view(NULL){
        frame_w=other.W();
        frame_h=other.H();
        frames_x=1;
        frames_y=1;
        horisontal=true;
        recent_view=&views["default"];
        recent_view->frame_count=1;
    }

    Sprite::Sprite(const Surface& other, const unsigned& frame_width, const unsigned& frame_height, const unsigned short& frames_in_x, const unsigned short& frames_in_y, bool hrs) : Surface(other), recent_view(NULL){
        frame_w=frame_width;
        frame_h=frame_height;
        frames_x=frames_in_x;
        frames_y=frames_in_y;
        horisontal=hrs;
        recent_view=&views["default"];
        recent_view->frame_count=(frames_x*frames_y);
    }

    Sprite::Sprite(const std::string path, const unsigned& frame_width, const unsigned& frame_height, const unsigned short& frames_in_x, const unsigned short& frames_in_y, bool hrs) : Surface(Surface::LoadIMG(path)), recent_view(NULL){
        frame_w=frame_width;
        frame_h=frame_height;
        frames_x=frames_in_x;
        frames_y=frames_in_y;
        horisontal=hrs;
        recent_view=&views["default"];
        recent_view->frame_count=(frames_x*frames_y);
    }


    Sprite::Sprite() : Surface(), frame_w(0), frame_h(0), frames_x(0), frames_y(0), horisontal(0), recent_view(NULL) {;}

    Sprite& Sprite::operator[](const std::string& rhs){
        recent_view=&views[rhs];
        return *this;
    }

    Sprite& Sprite::operator=(const Sprite& rhs){
        this->Surface::operator=(rhs);
        frame_w=rhs.frame_w;
        frame_h=rhs.frame_h;
        frames_x=rhs.frames_x;
        frames_y=rhs.frames_y;
        horisontal=rhs.horisontal;
        recent_view=&views["default"];
        recent_view->frame_count=(frames_x*frames_y);
        return *this;
    }


    void Sprite::Set(const unsigned& frame_width, const unsigned& frame_height, const unsigned short& frames_in_x, const unsigned short& frames_in_y, bool hrs){
        frame_w=frame_width;
        frame_h=frame_height;
        frames_x=frames_in_x;
        frames_y=frames_in_y;
        horisontal=hrs;
        recent_view=&views["default"];
        recent_view->frame_count=(frames_x*frames_y);
    }

    void Sprite::Set(const Surface& other, const unsigned& frame_width, const unsigned& frame_height, const unsigned short& frames_in_x, const unsigned short& frames_in_y, bool hrs){
        this->Face::operator=(other);
        frame_w=frame_width;
        frame_h=frame_height;
        frames_x=frames_in_x;
        frames_y=frames_in_y;
        horisontal=hrs;
        recent_view=&views["default"];
        recent_view->frame_count=(frames_x*frames_y);
    }

    void Sprite::Set(const std::string path, const unsigned& frame_width, const unsigned& frame_height, const unsigned short& frames_in_x, const unsigned short& frames_in_y, bool hrs){
        SelfLoad(path);
        frame_w=frame_width;
        frame_h=frame_height;
        frames_x=frames_in_x;
        frames_y=frames_in_y;
        horisontal=hrs;
        recent_view=&views["default"];
        recent_view->frame_count=(frames_x*frames_y);
    }

    void Sprite::SetView(const unsigned short& s_line, const unsigned short& s_frame_count, const unsigned short& s_frame_no){
        recent_view->line=s_line;
        recent_view->frame_count=s_frame_count;
        recent_view->frame_no=s_frame_no;
    }

    bool Sprite::Next(unsigned short move){
        recent_view->frame_no+=move;
        if(recent_view->frame_no%recent_view->frame_count!=recent_view->frame_no){
            recent_view->frame_no%=recent_view->frame_count;
            return true;
        }
        return false;
    }

}
