/*
 * MAsdl - C++ wrapper of SDL and extensional libs.
 * Copyright © 2013 Martin Zahradníček <martin.zahradniceks@gmail.com>
 *
 * This file is part of MAsdl.
 *
 * MAsdl is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MAsdl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAsdl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _MASDL_SPRITE_
#define _MASDL_SPRITE_

#include <string>
#include <map>
#include "surface.hpp"
#include "rect.hpp"

namespace MAsdl{

    class Sprite : public Surface{
        protected:
            unsigned frame_w, frame_h;
            unsigned short frames_x, frames_y;
            bool horisontal;
            class View{
                public:
                    unsigned short line;
                    unsigned short frame_count;
                    unsigned short frame_no;

                    View(unsigned short s_line=0, unsigned short s_frame_count=0, unsigned short s_frame=0);
                };
            std::map<std::string, View> views;
            View* recent_view;

            virtual void SetRect(Rect& src, bool IsNull);
        public:
            Sprite(const Sprite& other);
            Sprite(const Surface& other);
            Sprite(const Surface& other, const unsigned& frame_width, const unsigned& frame_height, const unsigned short& frames_in_x, const unsigned short& frames_in_y, bool hrs=true);
            Sprite(const std::string path, const unsigned& frame_width, const unsigned& frame_height, const unsigned short& frames_in_x, const unsigned short& frames_in_y, bool hrs=true);
            Sprite();
            Sprite& operator[](const std::string& rhs);
            Sprite& operator=(const Sprite& rhs);
            void Set(const unsigned& frame_width, const unsigned& frame_height, const unsigned short& frames_in_x, const unsigned short& frames_in_y, bool hrs);
            void Set(const Surface& other, const unsigned& frame_width, const unsigned& frame_height, const unsigned short& frames_in_x, const unsigned short& frames_in_y, bool hrs=true);
            void Set(const std::string path, const unsigned& frame_width, const unsigned& frame_height, const unsigned short& frames_in_x, const unsigned short& frames_in_y, bool hrs=true);
            void SetView(const unsigned short& s_line, const unsigned short& s_frame_count, const unsigned short& s_frame_no);
            bool Next(unsigned short move=1);
    };

}

#endif
