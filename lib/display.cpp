/*
 * MAsdl - C++ wrapper of SDL and extensional libs.
 * Copyright © 2013 Martin Zahradníček <martin.zahradniceks@gmail.com>
 *
 * This file is part of MAsdl.
 *
 * MAsdl is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MAsdl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAsdl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <iostream>
#include <SDL/SDL_video.h>
#include "display.hpp"

namespace MAsdl{

    Display::Display(int width, int height, Uint32 flags, Uint8 bpp):Face(5,5,8,SDL_SWSURFACE){
        SetVideo(width, height, flags, bpp);
    }

    Display::Display():Face(){}

    bool Display::IsAct(){
        if(id==SDL_GetVideoSurface())return true;
        return false;
    }

    bool Display::SetVideo(int width, int height,  Uint32 flags, Uint8 bpp){
        const SDL_VideoInfo* info=SDL_GetVideoInfo();
        if(bpp==0){bpp=info->vfmt->BitsPerPixel;}
        id=SDL_SetVideoMode(width, height, bpp, flags | SDL_ANYFORMAT);
        if(id==NULL){
            std::cerr<<"Unable to set video mode:\n  width: "<<width<<"\n  height: "<<height<<"\n  flags: "<<(flags | SDL_ANYFORMAT)<<std::endl;
            return false;
        }
        return true;
    }

    bool Display::Update(const Rect& rect){
        if(IsAct()){
            if(id->flags & SDL_DOUBLEBUF){
                return (SDL_Flip(id)==0?true:false);
            }
            else{
                SDL_UpdateRect(id, rect.rect.x, rect.rect.y, rect.rect.w, rect.rect.h);
                return true;
            }
        }
    }

    void Display::SetCaption(const std::string& title, const std::string& icon_name){
        SDL_WM_SetCaption(title.c_str(), icon_name.c_str());
    }

    void Display::SetTitle(const std::string& title){
        SDL_WM_SetCaption(title.c_str(), NULL);
    }

    /*bool Display::SetIcon(const std::string& icon_path){
        return (==0:true:false);
    }*/

}
