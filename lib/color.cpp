/*
 * MAsdl - C++ wrapper of SDL and extensional libs.
 * Copyright © 2013 Martin Zahradníček <martin.zahradniceks@gmail.com>
 *
 * This file is part of MAsdl.
 *
 * MAsdl is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MAsdl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAsdl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <iomanip>
#include <SDL/SDL_video.h>
#include "color.hpp"

namespace MAsdl{

    extern SDL_PixelFormat* DefaultPF;

    Color::Color(unsigned char sr, unsigned char sg, unsigned char sb, unsigned char sa):r(sr), g(sg), b(sb), a(sa){
    }

    Color::Color():r(0), g(0), b(0), a(255){}

    Color& Color::operator=(const Color& other){
        r=other.r;
        g=other.g;
        b=other.b;
        a=other.a;
        return *this;
    }

    bool Color::operator==(const Color& other)const{
        if(r==other.r&&g==other.g&&b==other.b&&a==other.a)return true;
        return false;
    }

    std::ostream& operator<< (std::ostream& os, const Color& rhs){
        std::ios_base::fmtflags flgs=os.flags();
        os<<"#"<<std::hex<< +rhs.r<<" "<< +rhs.g<<" "<< +rhs.b<<" "<< +rhs.a;
        os.flags(flgs);
        return os;
    }

    namespace col{
        Color Black(0, 0, 0);
        Color Red(255, 0, 0);
        Color Green(0, 255, 0);
        Color Blue(0, 0, 255);
        Color Yellow(255, 255, 0);
        Color Cyan(0, 255, 255);
        Color Purple(255, 0, 255);
        Color White(255, 255, 255);

        Color DarkRed(128, 0, 0);
        Color DarkGreen(0, 128, 0);
        Color DarkBlue(0, 0, 128);
        Color Vomit(128, 128, 0);
        Color DarkCyan(0, 128, 128);
        Color Magenta(128, 0, 128);
        Color Orange(255, 128, 0);
        Color Gray(128, 128, 128);

        Color & Olive = Vomit;
    }
}
