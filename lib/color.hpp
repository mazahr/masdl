/*
 * MAsdl - C++ wrapper of SDL and extensional libs.
 * Copyright © 2013 Martin Zahradníček <martin.zahradniceks@gmail.com>
 *
 * This file is part of MAsdl.
 *
 * MAsdl is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MAsdl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAsdl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _MASDL_COLOR_
#define _MASDL_COLOR_

#include <ostream>

namespace MAsdl{

    /** Class for easy storing color in RGBA format.
     */

    class Color{
        public:
            unsigned char r;
            unsigned char g;
            unsigned char b;
            unsigned char a;

            /** Creates new Color object with each color channel set by arguments.
             *
             *@param sr Value of red channel.
             *@param sg Value of green channel.
             *@param sb Value of blue channel.
             *@param sa Value of alpha channel.
             */
            Color(unsigned char sr, unsigned char sg, unsigned char sb, unsigned char sa=255);

            /** Creates new Color object with all values set to 0.
             */
            Color();

            /** Assigns each value of rhs to responding.
             *
             *@param rhs Assigned object.
             */
            Color& operator=(const Color& rhs);

            /** Compares values of Colors.
             *
             *@param rhs Compared object.
             *@return Returns true if each value eqals to responding.
             */
            bool operator==(const Color& rhs)const;
    };

    /** Outputs color.
     *
     * Not working yet.
     */
    std::ostream& operator<< (std::ostream& os, const Color& rhs);

    // Some default colors

    namespace col{
        extern Color Black;
        extern Color Red;
        extern Color Green;
        extern Color Blue;
        extern Color Yellow;
        extern Color Cyan;
        extern Color Purple;
        extern Color White;

        extern Color DarkRed;
        extern Color DarkGreen;
        extern Color DarkBlue;
        extern Color Vomit;
        extern Color DarkCyan;
        extern Color Magenta;
        extern Color Orange;
        extern Color Gray;

        extern Color & Olive;
    }
}

#endif
