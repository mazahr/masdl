/*
 * MAsdl - C++ wrapper of SDL and extensional libs.
 * Copyright © 2013 Martin Zahradníček <martin.zahradniceks@gmail.com>
 *
 * This file is part of MAsdl.
 *
 * MAsdl is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MAsdl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAsdl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <iostream>
#include <SDL/SDL_timer.h>
#include "fps.hpp"

namespace MAsdl{

    FPS::FPS():last_ticks(0), frames(0), interval(0), debt(0), running(0), debtful(0){;}

    FPS::FPS(int fps, int frame, bool debtfulness):last_ticks(0), frames(frame), interval(1000/fps), debt(0), running(0), debtful(debtfulness){}

    int FPS::Delay(){
        if(!running)return 0;
        int new_ticks=SDL_GetTicks();
        frames++;
        if((new_ticks-last_ticks)>interval){
            std::cerr<<"Your time is over ("<<(new_ticks-last_ticks)-interval<<" ms).";
            if(debtful)debt=(new_ticks-last_ticks)-interval;
            last_ticks=new_ticks;
            return -debt;
        }
        else{
            if(debtful){
                last_ticks=((debt<interval?(last_ticks+(interval-debt)):new_ticks));
                SDL_Delay(last_ticks-new_ticks);
                debt=0;
                return (last_ticks-new_ticks);
            }
            else{
                last_ticks=((new_ticks-last_ticks)<interval?(last_ticks+interval):new_ticks);
                SDL_Delay(last_ticks-new_ticks);
                return (last_ticks-new_ticks);
            }
        }
    }

    void FPS::Stop(){
        frames=0;
        running=0;
        debt=0;
    }

    void FPS::Pause(){
        running=0;
        debt=0;
    }

    void FPS::Start(){
        running=true;
        last_ticks=SDL_GetTicks();
    }

    void FPS::Restart(){
        Stop();
        Start();
    }

    void FPS::SetFPS(const unsigned& fps){
        interval=(1000/fps);
    }

    void FPS::SetInterval(const unsigned& inter){
        interval=inter;
    }

    void FPS::SetFrames(int frame){
        frames=frame;
    }

    int FPS::FrameNo(){
        return frames;
    }

}
