/*
 * MAsdl - C++ wrapper of SDL and extensional libs.
 * Copyright © 2013 Martin Zahradníček <martin.zahradniceks@gmail.com>
 *
 * This file is part of MAsdl.
 *
 * MAsdl is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MAsdl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAsdl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "rect.hpp"

namespace MAsdl{

    Rect::Rect(Sint16 sx, Sint16 sy, Uint16 sw, Uint16 sh):x(rect.x),y(rect.y),w(rect.w),h(rect.h){
        rect.x=sx;
        rect.y=sy;
        rect.w=sw;
        rect.h=sh;
    }

    Rect::Rect():x(rect.x),y(rect.y),w(rect.w),h(rect.h){x=y=w=h=0;}

    Rect& Rect::operator=(const Rect& other){
        rect=other.rect;
        return *this;
    }

    bool Rect::operator==(const Rect& other)const{
        if(x==other.x&&y==other.y&&w==other.w&&h==other.h)return true;
        return false;
    }

}
