/*
 * MAsdl - C++ wrapper of SDL and extensional libs.
 * Copyright © 2014 Adam Matoušek <adamatousek curlything gmail dot com>
 *
 * This file is part of MAsdl.
 *
 * MAsdl is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MAsdl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAsdl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef FONT_H_56S9QD8I
#define FONT_H_56S9QD8I

#include "elf.hpp"
#include <SDL/SDL_ttf.h>
#include <string>

namespace MAsdl{

    class Font
    {
        private:
            TTF_Font *id;
            bool InitMaybe();

        public:
            Font();
            Font(std::string path, unsigned size);
            virtual ~Font();

            /** Loads a new font
             *
             * Deletes the previous one. Returns true if font is loaded alright
             * @param size Point size (at 72 DPI???)
             */
            bool Load(std::string path, unsigned size);
            
            bool OK() const;
            bool operator!() const;

            /** Render text
             *
             * Creates a new surface with the given text (UTF-8). You are
             * responsible for deleting this surface. When "changing" the text,
             * don't forget to delete the surface first! If the font hasn't been
             * loaded yet, returns NULL.
             *
             * @param text Text to be rendered.
             * @param c Text color. Ignores alpha (blame SDL)
             * @param antialiased Whether to use smoothing (RGBA, grayscale not
             *    used as I have little time and don't really care)
             */
            Elf* MakeText(std::string text, Color c, bool antialiased = 1);
    };

}

#endif /* end of include guard: FONT_H_56S9QD8I */
