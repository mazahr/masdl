/*
 * MAsdl - C++ wrapper of SDL and extensional libs.
 * Copyright © 2013 Martin Zahradníček <martin.zahradniceks@gmail.com>
 *
 * This file is part of MAsdl.
 *
 * MAsdl is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MAsdl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAsdl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <iostream>
#include <cmath>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include "masdl.hpp"

namespace MAsdl{

    bool initialised = 0;
    const Rect noRect;

    SDL_PixelFormat* DefaultPF = new SDL_PixelFormat;
    SDL_PixelFormat* DefaultAPF = new SDL_PixelFormat;

    bool Init(){
        std::cerr<<"Initialising SDL."<<std::endl;
        if((SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO)==-1)) {
            std::cerr<<"Could not initialize SDL: "<<SDL_GetError()<<std::endl;
            initialised = false;
            return true;
        }
        Uint32 flags=IMG_INIT_JPG|IMG_INIT_PNG;
        if(IMG_Init(flags) ^ flags){
            std::cerr<<"IMG_Init: Failed to init required jpg and png support!"<<std::endl;
            std::cerr<<"IMG_Init: "<<IMG_GetError()<<std::endl;
            initialised = false;
            return true;
        }
        *DefaultPF = *DefaultAPF = *(SDL_GetVideoInfo()->vfmt);
        DefaultAPF->Amask = ((DefaultPF->Rmask | DefaultPF->Gmask | DefaultPF->Bmask) ^ 0xFFFFFFFF);
        if(DefaultAPF->BitsPerPixel == 24){
            DefaultAPF->BitsPerPixel = 32;
            DefaultAPF->BytesPerPixel = 4;
        }
        int i;
        for(i=0; ((DefaultAPF->Amask>>i)%16) == 16; i++){;}
        DefaultAPF->Ashift = i;
        std::cerr<<"SDL initialised"<<std::endl;
        initialised = true;
        return false;
    }

    void Quit(){
        if(initialised){
            std::cerr<<"Quiting SDL."<<std::endl;
            IMG_Quit();
            SDL_Quit();
        }
        std::cerr<<"Quiting ..."<<std::endl;
    }

}
