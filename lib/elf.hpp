/*
 * MAsdl - C++ wrapper of SDL and extensional libs.
 * Copyright © 2013 Martin Zahradníček <martin.zahradniceks@gmail.com>
 *
 * This file is part of MAsdl.
 *
 * MAsdl is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MAsdl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAsdl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _MASDL_ELF_
#define _MASDL_ELF_

#include <string>
#include "sprite.hpp"
#include "rect.hpp"

namespace MAsdl{

    /** Class that extends Sprite to be movable easily.
     */

    class Elf : public Sprite {
        protected:
            int x;
            int y;
        public:

            /** Creates new Elf object around the same suface as the other object.
             *
             * Sprite's settings are copied (so the source picture is on the same adress but the Sprite settings are only by the same value).
             *
             *@param other The Sprite to be copied.
             */
            Elf(const Sprite& other);

            /** Creates new Elf object around the same suface as the other object with Sprite's settings set by arguments 
             *
             *@param other The Sprite to be copied.
             *@param frame_width Width of one frame.
             *@param frame_height Height of one frame.
             *@param frames_in_x Number of frmaes in x direction.
             *@param frames_in_y Number of frmaes in y direction.
             *@param hrs Sets whether the views are left-right (set true) or top-botttom (set false) oriented.
             */
            Elf(const Surface& other, const unsigned& frame_width, const unsigned& frame_height, const unsigned short& frames_in_x, const unsigned short& frames_in_y, bool hrs=true);

            /** Creates new Elf object sources by picture in the given path with Sprite's settings set by arguments 
             *
             *@param path The Sprite to be copied.
             *@param frame_width Width of one frame.
             *@param frame_height Height of one frame.
             *@param frames_in_x Number of frmaes in x direction.
             *@param frames_in_y Number of frmaes in y direction.
             *@param hrs Sets whether the views are left-right (set true) or top-botttom (set false) oriented.
             */
            Elf(const std::string path, const unsigned& frame_width, const unsigned& frame_height, const unsigned short& frames_in_x, const unsigned short& frames_in_y, bool hrs=true);

            /** Creates empty Elf with no source image and nothig set.
             */
            Elf();

            /** Set position of Elf to given one.
             *
             *@param x_dest X position to be set.
             *@param y_dest Y position to be set.
             */
            void Moveto(int x_dest, int y_dest);

            /** Change position of Elf by the given delta.
             *
             * Positive delta means moving right or down.
             *
             *@param x_delta Number which the x position should be changed by.
             *@param y_delta Number which the y position should be changed by.
             */
            void Move(int x_delta, int y_delta);

            /** Returns the x position.
             */
            const int& X();

            /** Set the x position to given one.
             *
             *@param x_dest X position to be set.
             */
            const int& X(int x_dest);

            /** Change x position of Elf by the given delta.
             *
             * Positive delta means moving right.
             *
             *@param x_delta Number which the x position should be changed by.
             */
            const int& XDel(int x_delta);

            /** Returns the y position.
             */
            const int& Y();

            /** Set the y position to given one.
             *
             *@param y_dest Y position to be set.
             */
            const int& Y(int y_dest);

            /** Change Y position of Elf by the given delta.
             *
             * Positive delta means moving down.
             *
             *@param y_delta Number which the y position should be changed by.
             */
            const int& YDel(int y_delta);

            /** Blit the sprite on destination in Elf's position.
             *
             *@param destination Face to be blitted on.
             */
            bool Draw(Face destination);
    };

}

#endif
