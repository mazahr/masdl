/*
 * MAsdl - C++ wrapper of SDL and extensional libs.
 * Copyright © 2013 Martin Zahradníček <martin.zahradniceks@gmail.com>
 *
 * This file is part of MAsdl.
 *
 * MAsdl is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MAsdl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAsdl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "funxor.hpp"

namespace MAsdl{

    Funxor::Funxor():active(true){;}
    bool Funxor::IsActive()const{return active;}
    void Funxor::SetActive(bool a){active = a;}
    bool Funxor::OnActive(Uint8 gain, Uint8 state){return false;}
    bool Funxor::OnKeyDown(SDLKey key, SDLMod mod, Uint8 state){return false;}
    bool Funxor::OnKeyUp(SDLKey key, SDLMod mod, Uint8 state){return false;}
    bool Funxor::OnMouseMotion(Uint16 x, Uint16 y, Sint16 xrel, Uint16 yrel, Uint8 state){return false;}
    bool Funxor::OnMbuttonDown(Uint8 button, Uint16 x, Uint16 y, Uint8 state){return false;}
    bool Funxor::OnMbuttonUp(Uint8 button, Uint16 x, Uint16 y, Uint8 state){return false;}
    bool Funxor::OnJoyAxisMotion(Uint8 which, Uint8 axis, Sint16 value){return false;}
    bool Funxor::OnJoyBallMotion(Uint8 which, Uint8 ball, Sint16 xrel, Sint16 yrel){return false;}
    bool Funxor::OnJoyHatMotion(Uint8 which, Uint8 hat, Uint8 value){return false;}
    bool Funxor::OnJbuttonDown(Uint8 which, Uint8 button, Uint8 state){return false;}
    bool Funxor::OnJbuttonUp(Uint8 which, Uint8 button, Uint8 state){return false;}
    bool Funxor::OnQuit(){return false;}
    bool Funxor::OnWM(){return false;}
    bool Funxor::OnResize(int w, int h){return false;}

}
