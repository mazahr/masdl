/*
 * MAsdl - C++ wrapper of SDL and extensional libs.
 * Copyright © 2013 Martin Zahradníček <martin.zahradniceks@gmail.com>
 *
 * This file is part of MAsdl.
 *
 * MAsdl is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MAsdl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAsdl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef _MASDL_FUNXOR_
#define _MASDL_FUNXOR_

#include <SDL/SDL_events.h>

namespace MAsdl{

    class Funxor{
        private:
            bool active;
        public:
            //Funxor controlling
            Funxor();
            bool IsActive()const; // Whether should the funxor handle any events
            void SetActive(bool a = true); // Sets the activeness
            //Event handlers
            virtual bool OnActive(Uint8 gain, Uint8 state);
            virtual bool OnKeyDown(SDLKey key, SDLMod mod, Uint8 state);
            virtual bool OnKeyUp(SDLKey key, SDLMod mod, Uint8 state);
            virtual bool OnMouseMotion(Uint16 x, Uint16 y, Sint16 xrel, Uint16 yrel, Uint8 state);
            virtual bool OnMbuttonDown(Uint8 button, Uint16 x, Uint16 y, Uint8 state);
            virtual bool OnMbuttonUp(Uint8 button, Uint16 x, Uint16 y, Uint8 state);
            virtual bool OnJoyAxisMotion(Uint8 which, Uint8 axis, Sint16 value);
            virtual bool OnJoyBallMotion(Uint8 which, Uint8 ball, Sint16 xrel, Sint16 yrel);
            virtual bool OnJoyHatMotion(Uint8 which, Uint8 hat, Uint8 value);
            virtual bool OnJbuttonDown(Uint8 which, Uint8 button, Uint8 state);
            virtual bool OnJbuttonUp(Uint8 which, Uint8 button, Uint8 state);
            virtual bool OnQuit();
            virtual bool OnWM();
            virtual bool OnResize(int w, int h);
    };

}

#endif
