/*
 * MAsdl - C++ wrapper of SDL and extensional libs.
 * Copyright © 2013 Martin Zahradníček <martin.zahradniceks@gmail.com>
 *
 * This file is part of MAsdl.
 *
 * MAsdl is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MAsdl is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAsdl.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <SDL/SDL_events.h>
#include <iostream>
#include "event.hpp"

namespace MAsdl{

    Event::Event(){;}

    void Event::Add(Funxor& src){
        data.insert(&src);
    }

    void Event::Remove(Funxor& src){
        if(data.count(&src)){
            data.erase(&src);
        }
    }

    bool Event::Next(){
        std::set<Funxor*>::iterator iter;
        SDL_Event recent;
        bool other=SDL_PollEvent(&recent);
        if(!other) return false;
        switch(recent.type){
            case SDL_ACTIVEEVENT:
                for(iter=data.begin();iter!=data.end();iter++)
                    if((*iter)->IsActive())(*iter)->OnActive(recent.active.gain, recent.active.state);
                break;
            case SDL_KEYDOWN:
                for(iter=data.begin();iter!=data.end();iter++)
                    if((*iter)->IsActive())(*iter)->OnKeyDown(recent.key.keysym.sym, recent.key.keysym.mod, recent.key.state);
                break;
            case SDL_KEYUP:
                for(iter=data.begin();iter!=data.end();iter++)
                    if((*iter)->IsActive())(*iter)->OnKeyUp(recent.key.keysym.sym, recent.key.keysym.mod, recent.key.state);
                break;
            case SDL_MOUSEMOTION:
                for(iter=data.begin();iter!=data.end();iter++)
                    if((*iter)->IsActive())(*iter)->OnMouseMotion(recent.motion.x, recent.motion.y, recent.motion.xrel, recent.motion.yrel, recent.motion.state);
                break;
            case SDL_MOUSEBUTTONDOWN:
                for(iter=data.begin();iter!=data.end();iter++)
                    if((*iter)->IsActive())(*iter)->OnMbuttonDown(recent.button.button, recent.button.x, recent.button.y, recent.button.state);
                break;
            case SDL_MOUSEBUTTONUP:
                for(iter=data.begin();iter!=data.end();iter++)
                    if((*iter)->IsActive())(*iter)->OnMbuttonUp(recent.button.button, recent.button.x, recent.button.y, recent.button.state);
                break;
            case SDL_JOYAXISMOTION:
                for(iter=data.begin();iter!=data.end();iter++)
                    if((*iter)->IsActive())(*iter)->OnJoyAxisMotion(recent.jaxis.which, recent.jaxis.axis, recent.jaxis.value);
                break;
            case SDL_JOYBALLMOTION:
                for(iter=data.begin();iter!=data.end();iter++)
                    if((*iter)->IsActive())(*iter)->OnJoyBallMotion(recent.jball.which, recent.jball.ball, recent.jball.xrel, recent.jball.yrel);
                break;
            case SDL_JOYHATMOTION:
                for(iter=data.begin();iter!=data.end();iter++)
                    if((*iter)->IsActive())(*iter)->OnJoyHatMotion(recent.jhat.which, recent.jhat.hat, recent.jhat.value);
                break;
            case SDL_JOYBUTTONDOWN:
                for(iter=data.begin();iter!=data.end();iter++)
                    if((*iter)->IsActive())(*iter)->OnJbuttonDown(recent.jbutton.which, recent.jbutton.button, recent.jbutton.state);
                break;
            case SDL_JOYBUTTONUP:
                for(iter=data.begin();iter!=data.end();iter++)
                    if((*iter)->IsActive())(*iter)->OnJbuttonUp(recent.jbutton.which, recent.jbutton.button, recent.jbutton.state);
                break;
            case SDL_QUIT:
                for(iter=data.begin();iter!=data.end();iter++)
                    if((*iter)->IsActive())(*iter)->OnQuit();
                break;
            case SDL_SYSWMEVENT:
                for(iter=data.begin();iter!=data.end();iter++)
                    if((*iter)->IsActive())(*iter)->OnWM();
                break;
            case SDL_VIDEORESIZE:
                for(iter=data.begin();iter!=data.end();iter++)
                    if((*iter)->IsActive())(*iter)->OnResize(recent.resize.w, recent.resize.h);
                break;
        }
        return other;
    }

}
